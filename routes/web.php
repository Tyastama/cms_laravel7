<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');
Route::get('register', 'AuthController@showFormRegister')->name('register');
Route::post('register', 'AuthController@register');
 
Route::group(['middleware' => 'auth'], function () {
 
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');
 
});

//route CRUD Merk
Route::get('/merk','MerkController@merk');
Route::get('/merk/tambah','MerkController@tambah');
Route::post('/storemerk','MerkController@storemerk');
Route::get('/merk/edit/{id}','MerkController@edit');
Route::get('/merk/hapus/{id}','MerkController@hapus');
Route::post('/merk/update','MerkController@update');

Route::get('/unit','UnitKantorJenisController@index');
Route::get('/unit/tambah','UnitKantorJenisController@tambah');
Route::post('/storeunit','UnitKantorJenisController@storeunit');
Route::get('/unit/edit/{id}','UnitKantorJenisController@edit');
Route::get('/unit/hapus/{id}','UnitKantorJenisController@hapus');
Route::post('/unit/update','UnitKantorJenisController@update');

Route::get('/item','ItemJenisController@Ij');
Route::get('/item/tambah','ItemJenisController@tambah');
Route::post('/storeIj','ItemJenisController@storeIj');
Route::get('/item/edit/{id}','ItemJenisController@edit');
Route::get('/item/hapus/{id}','ItemJenisController@hapus');
Route::post('/item/update','ItemJenisController@update');

Route::get('/supplier','SupplierController@index');
Route::get('/supplier/tambah','SupplierController@tambah');
Route::post('/storesupplier','SupplierController@storesupplier');
Route::get('/supplier/edit/{id}','SupplierController@edit');
Route::get('/supplier/hapus/{id}','SupplierController@hapus');
Route::post('/supplier/update','SupplierController@update');

Route::get('/mutasi','MutasiController@index');
Route::get('/mutasi/tambah','MutasiController@tambah');
Route::post('/store','MutasiController@store');
Route::get('/edit/{id}','MutasiController@edit');
Route::get('/hapus/{id}','MutasiController@hapus');
Route::post('/update','MutasiController@update');


//modal
//Route::get('/merk/modalmerk','MerkController@modalmerk');



