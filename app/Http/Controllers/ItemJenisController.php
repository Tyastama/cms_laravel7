<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemJenisController extends Controller
{

        public function Ij()
        {
            $jenis = DB::table('item_jenis')->get();

            return view('item',['item_jenis' => $jenis]);
        }

        public function tambah()
        {


        return view('item/tambah');

        }
        public function storeIj(Request $request)
        {

            DB::table('item_jenis')->insert([
                'deskripsi_jenis' => $request->deskripsi_jenis,

            ]);

            return redirect('item');

        }


        public function edit($id)
        {

            $jenis = DB::table('item_jenis')->where('id_jenis_item',$id)->get();

            return view('item/edit',['item_jenis' => $jenis]);

        }
        public function update(Request $request)
    {

        DB::table('item_jenis')->where('id_jenis_item',$request->id)->update([
            'deskripsi_jenis' => $request->deskripsi_jenis,

        ]);

        return redirect('/');
    }

        public function hapus($id)
        {

            DB::table('item_jenis')->where('id_jenis_item',$id)->delete();


            return redirect('/');
        }
  }
