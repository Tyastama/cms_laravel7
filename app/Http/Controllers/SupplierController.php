<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function index()
    {
        $supplier = DB::table('supplier_client')->get();

    	return view('supplier',['supplier_client' => $supplier]);
    }

    public function tambah()
    {
	return view('supplier/tambah');
    }

    public function storesupplier(Request $request)
    {

        DB::table('supplier_client')->insert([
            'nama_supplier' => $request->nama_supplier,

        ]);

        return redirect('supplier');

    }


    public function edit($id)
    {

        $supplier = DB::table('supplier_client')->where('id_supplier',$id)->get();

        return view('supplier/edit',['supplier_client' => $supplier]);

    }

    public function update(Request $request)
    {

        DB::table('supplier_client')->where('id_supplier',$request->id)->update([
            'nama_supplier' => $request->nama_supplier,

        ]);

        return redirect('/');
    }

    public function hapus($id)
    {

        DB::table('supplier_client')->where('id_supplier',$id)->delete();


        return redirect('/');
    }

    
}
