<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MutasiController extends Controller
{

    public function index()
    {
        $rekap_item = DB::select('SELECT
                                    r.id_trans_rekap,
                                    r.id_jenis_unit,
                                    j.deskripsi_jenis,
                                    r.id_merk_unit,
                                    m.deskripsi_merk,
                                    r.tipe_unit,
                                    r.jml,
                                    r.no_usulan,
                                    r.tgl_acc_usulan,
                                    r.nominal_acc_usulan,
                                    r.tgl_beli,
                                    r.id_supplier,
                                    s.nama_supplier,
                                    r.harga_item,
                                    r.id_unit_kantor,
                                    u.unit_kantor,
                                    r.untuk_bagian,
                                    r.label_item,
                                    r.tgl_serah_terima,
                                    r.no_invoice,
                                    r.uraian_invoice,
                                    r.keterangan,
                                    r.lokasi_barang 
                                FROM
                                    rekap_item AS r
                                    LEFT JOIN item_jenis AS j ON j.id_jenis_item = r.id_jenis_unit
                                    LEFT JOIN item_merk AS m ON m.id_merk_item = r.id_merk_unit
                                    LEFT JOIN supplier_client AS s ON s.id_supplier = r.id_supplier
                                    LEFT JOIN unit_kantor AS u ON u.id_unit = r.id_unit_kantor 
                                ORDER BY
                                    r.id_trans_rekap ASC');
        return view('mutasi', ['rekap_item' => $rekap_item ]);
    }

    public function tambah()
    {

	return view('mutasi/tambah');

    }
    public function store(Request $request)
    {

        DB::table('rekap_item')->insert([
            'id_jenis_unit'         => $request -> id_jenis_unit,
            'id_merk_unit'          => $request -> id_merk_unit,
            'tipe_unit'             => $request -> tipe_unit,
            'jml'                   => $request -> jml,
            'no_usulan'             => $request -> no_usulan,
            'tgl_acc_usulan'        => $request -> tgl_acc_usulan,
            'nominal_acc_usulan'    => $request -> nominal_acc_usulan,
            'tgl_beli'              => $request -> tgl_beli,
            'id_supplier'           => $request -> id_supplier,
            'harga_item'            => $request -> harga_item,
            'id_unit_kantor'        => $request -> id_unit_kantor,
            'untuk_bagian'          => $request -> untuk_bagian,
            'label_item'            => $request -> label_item,
            'tgl_serah_terima'      => $request -> tgl_serah_terima,
            'no_invoice'            => $request -> no_invoice,
            'uraian_invoice'        => $request -> uraian_invoice,
            'keterangan'            => $request -> keterangan,
            'lokasi_barang'         => $request -> lokasi_barang,
        ]);

        return redirect('mutasi');

    }
    public function edit($id)
    {

        $rekap_data = DB::table('rekap_item')->where('id_trans_rekap',$id)->get();

        return view('/mutasi/edit',['rekap_item' => $rekap_data]);

    }
    public function update(Request $request)
    {

        DB::table('rekap_item')->where('id_trans_rekap',$request->id)->update([
            'id_jenis_unit'         => $request -> id_jenis_unit,
            'id_merk_unit'          => $request -> id_merk_unit,
            'tipe _unit'            => $request -> tipe_unit,
            'jml'                   => $request -> jml,
            'no_usulan'             => $request -> no_usulan,
            'tanggal_acc_usulan'    => $request -> tanggal_acc_usulan,
            'nominal_acc_usulan'    => $request -> nominal_acc_usulan,
            'tanggal_beli'          => $request -> tanggal_beli,
            'id_supplier'           => $request -> id_supplier,
            'harga_item'            => $request -> harga_item,
            'id_unit_kantor'        => $request -> id_unit_kantor,
            'untuk_bagian'          => $request -> untuk_bagian,
            'label_item'            => $request -> label_item,
            'tanggal_serah_terima'  => $request -> tanggal_serah_terima,
            'no_invoice'            => $request -> no_invoice,
            'uraian_invoice'        => $request -> uraian_invoice,
            'keterangan'            => $request -> keterangan,
            'lokasi_barang'         => $request -> lokasi_barang,
            ]);

        return redirect('/');
    }
    public function hapus($id)
    {

        DB::table('rekap_item')->where('id_trans_rekap',$id)->delete();


        return redirect('/');
    }

   
    
}
