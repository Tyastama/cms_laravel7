<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitKantorJenisController extends Controller
{
    public function index()
    {
        $UnitKantorJenis = DB::table('unit_kantor')->get();

    	return view('unit',['unit_kantor' => $UnitKantorJenis]);
    }

    public function tambah()
    {

	return view('unit/tambah');

    }
    public function storeunit(Request $request)
    {

        DB::table('unit_kantor')->insert([
            'unit_kantor' => $request->unit_kantor,

        ]);

        return redirect('unit');

    }


    public function edit($id)
    {

        $UnitKantorJenis = DB::table('unit_kantor')->where('unit_kantor',$id)->get();

        return view('unit/edit',['unit_kantor' => $UnitKantorJenis]);

    }
    public function update(Request $request)
    {

        DB::table('unit_kantor')->where('id_unit',$request->id)->update([
            'unit_kantor' => $request->unit_kantor,

        ]);

        return redirect('unit');
    }

    public function hapus($id)
    {

        DB::table('unit_kantor')->where('id_unit',$id)->delete();


        return redirect('unit');
    }



   
}
