<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MerkController extends Controller
{
    public function merk()
    {
        $merk = DB::table('item_merk')->get();

    	return view('index',['item_merk' => $merk]);
    }

    public function tambah()
    {

	return view('merk/tambah');

    }
    public function storemerk(Request $request)
    {

        DB::table('item_merk')->insert([
            'deskripsi_merk' => $request->deskripsi_merk,
        ]);

        return redirect('index');

    }
    public function edit($id)
    {

        $merk = DB::table('item_merk')->where('id_merk_item',$id)->get();

        return view('/merk/edit',['item_merk' => $merk]);

    }
    public function update(Request $request)
    {

        DB::table('item_merk')->where('id_merk_item',$request->id)->update([
            'deskripsi_merk' => $request->deskripsi_merk,

        ]);

        return redirect('/');
    }
    public function hapus($id)
    {

        DB::table('item_merk')->where('id_merk_item',$id)->delete();


        return redirect('/');
    }
    // public function modalmerk()
    // {
    //     $merk = DB::table('item_merk')->get();

    // 	return view('merk/modalmerk',['item_merk' => $merk]); 
    // }
}
