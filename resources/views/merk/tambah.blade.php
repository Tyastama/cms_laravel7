@extends('template')
@section('content')
	<h3>Data Merk </h3>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <form action="/storemerk" method="post">
                <div class="form-group">
                {{ csrf_field() }}
                 Deskripsi Merk <input type="text" name="deskripsi_merk" required="required"> <br/>
                <br><br>
                <button type="submit" class="btn btn-primary"> Simpan </button>
                <a class="btn btn-primary" href="/" role="button">Batal</a>
                </div>
            </form>
        </div>
    </div>
@endsection
