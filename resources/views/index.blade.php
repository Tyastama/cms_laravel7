@extends('template')
@section('content')
<div class="container">
<div class="card ">
              <div class="card-header">
                <h3 class="card-title">Data Merk</h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
              <a class="btn btn-primary" href="/merk/tambah" role="button" >Tambah</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Deskripsi item</th>
                    <th>Action</th>

                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                        @foreach($item_merk as $p)
                        <td >{{ $p->deskripsi_merk }}</td>
                        <td >
                            <a class="btn btn-primary btn-right" href="/merk/edit/{{ $p->id_merk_item }}" role="button">Edit</a>
                            <a class="btn btn-primary btn-right" href="/merk/hapus/{{ $p->id_merk_item }}" role="button">Hapus</a>
                        </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
</div>
@endsection
