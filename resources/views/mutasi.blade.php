@extends('template')
@section('content')
<div class="container">
<div class="card ">
              <div class="card-header">
                <h3 class="card-title">Mutasi Data</h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
              <a class="btn btn-primary" href="mutasi/tambah" role="button" ><i class="fas fa-plus"></i> Add Rekap </a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>id trans rekap</th>
                    <th>id jenis unit</th>
                    <th>id merk_unit</th>
                    <th>tipe unit</th>
                    <th>jml</th>
                    <th>no_usulan</th>
                    <th>tanggal_acc_usulan</th>
                    <th>nominal_acc_usulan</th>
                    <th>tanggal_beli</th>
                    <th>id_supplier</th>
                    <th>harga_item</th>
                    <th>id_unit_kantor</th>
                    <th>untuk_bagian</th>
                    <th>label_item</th>
                    <th>tanggal_serah_terima</th>
                    <th>no_invoice</th>
                    <th>uraian_invoice</th>
                    <th>keterangan</th>
                    <th>lokasi_barang</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                        @foreach($rekap_item as $p)
                        <td>{{ $p->id_trans_rekap }}</td>
                        <td>{{ $p->deskripsi_jenis }}</td>
                        <td >{{ $p->deskripsi_merk }}</td>
                        <td>{{ $p->tipe_unit }}</td>
                        <td>{{ $p->jml }}</td>
                        <td>{{ $p->no_usulan }}</td>
                        <td >{{ $p->tgl_acc_usulan }}</td>
                        <td>{{ $p->nominal_acc_usulan }}</td>
                        <td>{{ $p->tgl_beli }}</td>
                        <td>{{ $p->nama_supplier }}</td>
                        <td >{{ $p->harga_item }}</td>
                        <td>{{ $p->unit_kantor }}</td>
                        <td>{{ $p->untuk_bagian }}</td>
                        <td>{{ $p->label_item }}</td>
                        <td >{{ $p->tgl_serah_terima }}</td>
                        <td>{{ $p->no_invoice }}</td>
                        <td>{{ $p->uraian_invoice }}</td>
                        <td>{{ $p->keterangan }}</td>
                        <td >{{ $p->lokasi_barang }}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
</div>
@endsection
