@extends('template')
@section('content')
<div>
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- form start -->
             <form  action="/store" method="post" >
                 
                <div class="card-body">
                  <div class="form-group">
                    {{ csrf_field() }}
                      <div class="row">
                          <!-- inputan code unit -->
                          <div class="col-md-4">
                                  <div class="input-group">                              
                                      <input type="text" class="form-control" name="id_jenis_unit" placeholder="Id Jenis Unit">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                                  
                         </div>

                          <div class="col-md-4">
                              <div class="form-group">
 
                                  <div class="input-group">                              
                                      <input type="text" class="form-control" name="id_merk_unit" placeholder="Id Merk Unit">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal4">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="tipe_unit" placeholder="Tipe Unit">
                                    <div class="input-group-prepend">
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div> 

                        <div class="row">
                          <!-- inputan code unit -->
                          <div class="col-md-4">
                              <div class="form-group">
                                  <div class="input-group">                              
                                      <input type="text" class="form-control" name="jml" placeholder="Jumlah">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                   <div class="input-group">                              
                                      <input type="text" class="form-control" name="no_usulan" placeholder="No Usulan">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  
                                  <div class="input-group">                              
                                      <input type="text" class="form-control" name="tgl_acc_usulan"  placeholder="Tanggal Acc Usulan">
                                      <div class="input-group-prepend">
                                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-calendar-times"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div> 

                        <div class="row">
                          <!-- inputan code unit -->
                          <div class="col-md-4">
                              <div class="form-group">
                                  
                                  <div class="input-group">                              
                                      <input type="text" class="form-control" name="nominal_acc_usulan" placeholder="nominal Acc Usulan">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="tgl_beli" placeholder="Tanggal Beli">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-calendar-times"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="id_supplier" placeholder="Id Supplier">
                                      <div class="input-group-prepend">
                                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div> 


                        <div class="row">
                          <!-- inputan code unit -->
                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="harga_item" placeholder="Harga Item">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="id_unit_kantor" placeholder="Id Unit Kantor">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="untuk_bagian" placeholder="Bagian">
                                      <div class="input-group-prepend">
                                      
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div> 

                        <div class="row">
                          <!-- inputan code unit -->
                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="label_item" placeholder="label Item">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="tgl_serah_terima" placeholder="Tanggal Serah Terima">
                                      <div class="input-group-prepend">
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-calendar-times"></i>
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="no_invoice" placeholder="No Invoice">
                                      <div class="input-group-prepend">
                                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#">
                                          <i class="fas fa-plus"></i>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div> 

                        <div class="row">
                          <!-- inputan code unit -->
                          <div class="col-md-4">
                              <div class="form-group">
                                    <div class="input-group">                              
                                      <input type="text" class="form-control" name="uraian_invoice" placeholder="Urain Invoice">
                                      <div class="input-group-prepend">
                                          
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                   <div class="input-group">                              
                                      <input type="text" class="form-control" name="keterangan" placeholder="Keterangan">
                                      <div class="input-group-prepend">
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                   <div class="input-group">                              
                                      <input type="text" class="form-control" name="lokasi_barang" placeholder="Lokasi Barang">
                                      <div class="input-group-prepend">
                                      
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div> 

                  </div>    
    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" value="Simpan Data">Submit</button>
                  <button type="submit" class="btn btn-primary" value="Simpan Data" herf="/">Batal</button>
                </div>
              </form>
             </div>
            <!-- /.card -->
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
       <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
        
         </div>
      </div>
 
    </div>
  </div>


  <!-- Modal -->
<div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body">
        
        </div>
        
      </div>
      
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="myModal4" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
        
        </div>
        
      </div>
      
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="myModal5" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
        <table class="table">
                  <thead>
                  <tr>
                    <th>id_merk_item</th>
                    <th>Deskripsi Merk</th>

                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                       
                    <td class="id_merk_item" id="id_merk_item" ></td>
                    <td class="deskripsi_merk" id="deskripsi_merk"></td>
                        
                  </tr>
                 
                  </tbody>
                </table>
        </div>
        
      </div>
      
    </div>
  </div>
@endsection
