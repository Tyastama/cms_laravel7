@extends('template')
@section('content')
	<h3>Data Unit </h3>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <form action="/storeunit" method="post">
                <div class="form-group">
                {{ csrf_field() }}
                Unit Kantor <input type="text" name="unit_kantor" required="required"> <br/>
                <br><br>
                <button type="submit" class="btn btn-primary"> Simpan </button>
                <a class="btn btn-primary" href="/" role="button">Batal</a>
                </div>
            </form>
        </div>
    </div>
@endsection
