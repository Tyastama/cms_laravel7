@extends('template')
@section('content')
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              @foreach($supplier_client as $p)
              <form  action="/update" method="post" >
              {{ csrf_field() }}
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInput">Nama Supplier</label>
                    <input type="hidden" name="id" value="{{ $p->id_supplier }}"> <br/>
		            <input type="text" required="required" name="deskripsi_merk" value="{{ $p->nama_supplier }}"> <br/>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" value="Simpan Data">Submit</button>
                  <button type="submit" class="btn btn-primary" value="Simpan Data" herf="/">Batal</button>
                </div>
              </form>
              @endforeach
            </div>
            <!-- /.card -->
        </div>
</div>
@endsection
