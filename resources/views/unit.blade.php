@extends('template')
@section('content')
<div class="container">
<div class="card ">
              <div class="card-header">
                <h3 class="card-title">Data Unit Kantor</h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
              <a class="btn btn-primary" href="/unit/tambah" role="button" >Tambah</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Unit Kantor</th>
                    <th>Action</th>

                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                        @foreach($unit_kantor as $p)
                        <td >{{ $p->unit_kantor }}</td>
                        <td >
                            <a class="btn btn-primary btn-right" href="/unit/edit/{{ $p->id_unit }}" role="button">Edit</a>
                            <a class="btn btn-primary btn-right" href="/unit/hapus/{{ $p->id_unit }}" role="button">Hapus</a>
                        </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
</div>
@endsection
