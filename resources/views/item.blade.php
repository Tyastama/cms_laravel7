@extends('template')
@section('content')
<div class="container">
<div class="card ">
              <div class="card-header">
                <h3 class="card-title">Data Item Jenis</h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
              <a class="btn btn-primary" href="/item/tambah" role="button" >Tambah</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Deskripsi item</th>
                    <th>Action</th>

                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                        @foreach($item_jenis as $p)
                        <td >{{ $p->deskripsi_jenis }}</td>
                        <td >
                            <a class="btn btn-primary btn-right" href="/item/edit/{{ $p->id_jenis_item }}" role="button">Edit</a>
                            <a class="btn btn-primary btn-right" href="/item/hapus/{{ $p->id_jenis_item }}" role="button">Hapus</a>
                        </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
</div>
@endsection
