@extends('template')
@section('content')
<div class="container">
<div class="card ">
              <div class="card-header">
                <h3 class="card-title">Data Supplier</h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
              <a class="btn btn-primary" href="/supplier/tambah" role="button" >Tambah</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Supplier</th>
                    <th>Action</th>

                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                        @foreach($supplier_client as $p)
                        <td >{{ $p->nama_supplier }}</td>
                        <td >
                            <a class="btn btn-primary btn-right" href="/supplier/edit/{{ $p->id_supplier }}" role="button">Edit</a>
                            <a class="btn btn-primary btn-right" href="/supplier/hapus/{{ $p->id_supplier}}" role="button">Hapus</a>
                        </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
</div>
@endsection
